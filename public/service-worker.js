
self.addEventListener('fetch', function(event) {
  event.respondWith(fetch(event.request));
});

/**
async function updateArticles() {
  navigator.setAppBadge(12);
}

self.addEventListener('periodicsync', (event) => {
  if (event.tag === 'jobad-sync') {
    event.waitUntil(updateArticles());
  }
});
**/
