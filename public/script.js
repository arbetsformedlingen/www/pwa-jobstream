const BLOOM_FILTER_KEY = 'bloom_filter';
const ADS_SAVED_KEY = 'ads_saved';
const ADS_CURRENT = 'ads_current';
const LAST_DATA_SYNC = 'last_data_sync';

var unread = 0;
var ads = [];
var ads_stream = [];
var saved_ads = [];
var last_sync;

var bloomFilter = new BloomFilter(10000);

// Load bloom filter from local storage
const savedFilter = localStorage.getItem(BLOOM_FILTER_KEY);
if (savedFilter) {
  bloomFilter = BloomFilter.fromJSON(JSON.parse(savedFilter));
}
// Load saved ads from local storage
const savedFilterAds = localStorage.getItem(ADS_SAVED_KEY);
if (savedFilterAds) {
  saved_ads = JSON.parse(savedFilterAds);
}

// Load saved ads from local storage
const savedStream = localStorage.getItem(ADS_CURRENT);
if (savedStream) {
  ads_stream = JSON.parse(savedStream);
}

const saveStream = () => {
  var stream = JSON.stringify(ads_stream);
  localStorage.setItem(ADS_CURRENT, stream);
};

const saveFilterToLocalStorage = () => {
  localStorage.setItem(BLOOM_FILTER_KEY, JSON.stringify(bloomFilter.toJSON()));
};

const saveSyncDate = () => {
  var date = last_sync.toJSON();
  localStorage.setItem(LAST_DATA_SYNC, date);
};

// Load saved ads from local storage
const savedFilterSync = localStorage.getItem(LAST_DATA_SYNC);
if (savedFilterSync) {
  last_sync = new Date(JSON.parse(JSON.stringify(savedFilterSync)));
}else{

  let promise = new Promise(function(resolve, reject) {
    resolve(sync());
});

  promise.then( () => {
    load();
  });

}

var queue = [];

function markViewed(){
  while (queue.length > 0) {
    let id = queue.shift();
    if(!bloomFilter.contains(id)){
      bloomFilter.add(id);
      unread--;
      var counter = document.getElementById('unread');
      counter.innerText = unread;
      navigator.setAppBadge(unread);
    }
  }
}

document.addEventListener("scroll", function() {
  markViewed();
});

var observer = new IntersectionObserver(function(entries) {
  // isIntersecting is true when element and viewport are overlapping
  // isIntersecting is false when element and viewport don't overlap
  entries.forEach( entry => {
    if (!entry.isIntersecting) {
      return;
    }else{
      var id = entry.target.id;
      queue.push(id);
    }
  })

});

function formatDate(date) {

  const datesAreOnSameDay = (first, second) =>
  first.getFullYear() === second.getFullYear() &&
  first.getMonth() === second.getMonth() &&
  first.getDate() === second.getDate();

  today = new Date();
  compDate = new Date(date);
  compDate = new Date(compDate.getTime() + compDate.getTimezoneOffset() * 60000);
  diff = today.getTime() - compDate.getTime();
  if (datesAreOnSameDay(today,compDate)) {
    return "Inläst idag, "+compDate.getHours()+":"+compDate.getMinutes();
  } else if (diff <= (24 * 60 * 60 *1000)) {
    return "Inläst igår, "+compDate.getHours()+":"+compDate.getMinutes();
  } else {
    return compDate.toDateString(); // or format it what ever way you want
  }
}

function ad2html(ad){
  let html = "";
  html += `
  <article id="${ad.id}">
  <h3>
  <a href="${ad.url}" target="_blank" rel="noopener">
  ${ad.title}
  </a>
  </h3>
  <p>${ad.description}</p>
  <p>
  <button id="${ad.id}">Spara</button>
  </p>
  </article>
  `;
  return html;
}


(function () {
  load();
  toggle(true);
})();


function load(){

  var stream = JSON.parse(localStorage.getItem(ADS_CURRENT));
  if(stream == null){
    return;
  }
  let html = "";
  var remove = [];
  var counter = 0

  stream.forEach(ad => {
    if(!bloomFilter.contains(ad.id)){
      ads.push(ad);
      html += ad2html(ad);
      counter++;
    }else{
      remove.push(ad.id);
    }
  });

  var filtered = stream.filter(function(el) { return remove.indexOf(el.id) <= 0 });
  localStorage.setItem(ADS_CURRENT, JSON.stringify(filtered));

  var jobList = document.getElementById('job-list');
  var updated = document.getElementById('updated');
  var unread2 = document.getElementById('unread');


  jobList.innerHTML = "";
  unread2.innerText = counter;

  unread = counter;

  if(last_sync){
    const humanReadableDateTime = formatDate(last_sync.toLocaleString());
    updated.innerText = humanReadableDateTime;
  }else{
    updated.innerText = "Sync pågår";
  }

  jobList.insertAdjacentHTML("beforeend", html);

  var allHtags = document.querySelectorAll("article");

  allHtags.forEach(tag => {
    observer.observe(tag);
  });

  // Add action on all butons
  const saveAd = (ad,arr) => {
    if (arr.filter(elem => elem.id == ad.id).length <= 0){
      arr.push(ad);
      // Saved ads
      var saved = document.getElementById('saved');
      saved.innerText = saved_ads.length;
    }
  }
  const btns = document.querySelectorAll('button')

  btns.forEach(btn => {

    btn.addEventListener('click', event => {

      ads.forEach(ad => {
        if(ad.id == event.target.id){
          console.log(ad);
          saveAd(ad,saved_ads);
          localStorage.setItem(ADS_SAVED_KEY, JSON.stringify(saved_ads));
        }
      })
    });

  });

}

function toggle(b){

  var name;
  if(b)
  name = "stream";
  else {
    name = "loadSavedAds";
  }

  var streamLink = document.getElementById("stream");
  var savedLink = document.getElementById("loadSavedAds");

  if (name == "stream"){
    streamLink.classList.add("active");
    savedLink.classList.remove("active");
  }else{
    streamLink.classList.remove("active");
    savedLink.classList.add("active");
  }

}

async function sync(){
  var date;

  if(last_sync)
  date = last_sync.toISOString();
  else {
    date = new Date(Date.now() - 20 * 24 * 3600 * 1000).toISOString();
  }

  var s = await fetch(`https://jobstream.api.jobtechdev.se/feed?date=${date}&occupation-concept-id=fg7B_yov_smw`);
  var r = await s.text();
  var data = await new window.DOMParser().parseFromString(r, "text/xml");

  const items = data.querySelectorAll("entry");

  items.forEach(el => {
    var url = el.querySelector("id").innerHTML;
    const id = url.split('/').pop();
    var title = el.querySelector("title").innerHTML;
    var description = el.querySelector("content").innerHTML;
    var ad = {id,title,url,description};

    ads_stream.push(ad);

  });

  saveStream();
  last_sync = new Date();
  saveSyncDate();

/**
  fetch(`https://jobstream.api.jobtechdev.se/feed?date=${date}&occupation-concept-id=fg7B_yov_smw`)
    .then(response => response.text())
    .then(str => new window.DOMParser().parseFromString(str, "text/xml"))
    .then(data => {

      const items = data.querySelectorAll("entry");

      items.forEach(el => {
        var url = el.querySelector("id").innerHTML;
        const id = url.split('/').pop();
        var title = el.querySelector("title").innerHTML;
        var description = el.querySelector("content").innerHTML;
        var ad = {id,title,url,description};

        ads_stream.push(ad);

      });

      saveStream();
      last_sync = new Date();
      saveSyncDate();

    }).catch( err => {
      console.log(err);
    });
**/
  }

  function setUpdated(){
    var updated = document.getElementById('updated');
    date = last_sync; // date[0].innerHTML
    const humanReadableDateTime = formatDate(date.toLocaleString());
    updated.innerText = humanReadableDateTime;
  }
  //setUpdated();

  // Saved ads
  var saved = document.getElementById('saved');
  saved.innerText = saved_ads.length;
  // Load saved ads
  function loadSavedAds(){

    var streamLink = document.getElementById("stream");
    streamLink.classList.remove("active");

    var savedLink = document.getElementById("loadSavedAds");
    savedLink.classList.add("active");

    var jobList = document.getElementById('job-list');
    jobList.innerHTML = "";
    let html = "";
    saved_ads.forEach( ad => {
      html += `
      <article id="${ad.id}">
      <h3>
      <a href="${ad.url}" target="_blank" rel="noopener">
      ${ad.title}
      </a>
      </h3>
      <p>${ad.description}</p>
      </article>
      `;
    });
    jobList.insertAdjacentHTML("beforeend", html);
  }

  function setBadge(){
    try{
      navigator.setAppBadge(unread);
    }catch(error){
      console.log("SetBadge icon not supported");
    }
  }

  setInterval(saveFilterToLocalStorage, 5000);
  setInterval(sync, 1000*60);
  setInterval(setBadge, 1000*20);

  var deferredPrompt;

  window.addEventListener('beforeinstallprompt', function(e) {
    console.log('beforeinstallprompt Event fired');
    e.preventDefault();

    // Stash the event so it can be triggered later.
    deferredPrompt = e;

    return false;
  });

  const installApp = document.getElementById('installApp');
  installApp.addEventListener('click', async () => {
    if (deferredPrompt) {
      deferredPrompt.prompt();
      const { outcome } = await deferredPrompt.userChoice;
      if (outcome === 'accepted') {
        deferredPrompt = null;
      }
    }
  });
